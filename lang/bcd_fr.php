<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/boutons_contacts_et_dates.git

return [

	// T
	'texte_ajouter_google' => 'Ajouter à Google Agenda',
	'texte_ajouter_ics' => 'Récupérer le rendez-vous',
	'texte_ajouter_vcf' => 'Récupérer la vCard',
];
