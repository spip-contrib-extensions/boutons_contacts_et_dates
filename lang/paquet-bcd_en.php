<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/bcd-paquet-xml-boutons_contacts_et_dates?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// B
	'bcd_description' => 'Using models, this plugin lets you add a button to a template or article to retrieve either an meeting in ics format, or a person’s business card in vcard format.
	The plugin automatically adds a vcard button to the author information box.',
	'bcd_nom' => 'Contact & meeting buttons',
	'bcd_slogan' => 'Add meeting or contact in one click',
];
