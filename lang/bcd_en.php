<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/bcd-boutons_contacts_et_dates?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// T
	'texte_ajouter_google' => 'Add to Google Calendar',
	'texte_ajouter_ics' => 'Get the appointment',
	'texte_ajouter_vcf' => 'Get the vCard',
];
