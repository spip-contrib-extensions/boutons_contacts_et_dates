<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/boutons_contacts_et_dates.git

return [

	// B
	'bcd_description' => 'Ce plugin permet, au travers de modèles, d’ajouter un bouton dans un squelette ou un article pour récupérer, soit un rendez-vous au format ics, soit la carte de visite d’une personne au format vcard.
	Le plugin ajoute automatiquement un bouton vcard dans la boite d’informations d’un auteur.',
	'bcd_nom' => 'Boutons contacts & rendez-vous',
	'bcd_slogan' => 'Ajouter en un clic rendez-vous ou contact',
];
