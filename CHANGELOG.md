# CHANGLOG du plugin bcd

## [Unreleased]

- Commit 2d0f04f2 : fix #10 - meilleure prise en compte de la localisation des évènements (plugin Agenda).
- Commit NC : fix #11 - Mise en conformité des fichiers de langue (fr) - Adopter la syntaxe valide à partir de SPIP 4.1 ce qui facilitera la migration en SPIP 5. Salvator modifiera le formatage des autres langues.
