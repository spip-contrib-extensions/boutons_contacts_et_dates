<?php
/**
 * Déclarations d'autorisations.
 *
 * @package SPIP\Ajouter\Autorisations
 **/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction du pipeline autoriser. N'a rien à faire.
 *
 * @pipeline autoriser
 */
function bcd_autoriser() {
}

/**
 * Autorisation relative au droit à la portabilité d'un auteur.
 *
 * Le droit à la portabilité donne aux personnes
 * la possibilité de récupérer une partie de leurs données
 * dans un format ouvert et lisible par machine.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
 **/
function autoriser_auteur_exportervcard_dist($faire, $type, $id, $qui, $opt) {
	// Seul l'auteur peut récupérer sa vCard.
	return $qui['id_auteur'] == $id;
}

/**
 * Autorisation relative au droit à l'export d'un évènement d'agenda.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
 **/
function autoriser_evenement_exporterrdv_dist($faire, $type, $id, $qui, $opt) {
	// Seul l'auteur peut récupérer sa vCard.
	return autoriser('voir', 'evenement', $id);
}
