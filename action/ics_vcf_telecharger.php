<?php
/**
 * Gestion de l'action ics_vcf_telecharger.
 *
 * @plugin     bcd
 *
 * @copyright  2022
 * @author     Vincent CALLIES
 *
 * @licence    MIT
 *
 * @package    SPIP\bcd\Action
 **/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action.
 *
 * L'argument attendu est le chemin et nom de l'archive transmis de façon sécurisée.
 *
 * @return void
 */
function action_ics_vcf_telecharger() : void {
	// Securisation de l'argument nom de l'archive
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$chemin_fichier = $securiser_action();

	// Si l'archive n'est pas accessible on renvoie une erreur
	if (!@is_readable($chemin_fichier)) {
		spip_log("Le fichier {$chemin_fichier} n'est pas accessible pour le téléchargement", 'bcd' . _LOG_ERREUR);
	} else {
		// Détermination de l'extension du fichier
		$extension = pathinfo($chemin_fichier, PATHINFO_EXTENSION);

		// Vider tous les tampons pour ne pas provoquer de Fatal memory exhausted
		$level = @ob_get_level();
		while ($level--) {
			@ob_end_clean();
		}

		// Telechargement du fichier d'archive
		header('Content-Description: File Transfer');
		header('Content-type: text/' . ($extension === 'ics' ? 'calendar' : 'vcard'));
		header('Content-Disposition: attachment; filename="' . basename($chemin_fichier) . '"');
		header('Content-Transfer-Encoding: binary');
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Length: ' . filesize($chemin_fichier));
		readfile($chemin_fichier);
		exit;
	}
}
