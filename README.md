# Boutons contacts & rendez-vous

Ce plugin permet, au travers de modèles, d'ajouter un bouton dans un squelette ou un article pour récupérer, soit un rendez-vous au format ics, soit la carte de visite d'une personne au format vcard.
Le plugin ajoute également automatiquement un bouton vcard dans la boite d'informations d'un
auteur SPIP.

Pour l'exemple, ci-dessous des appels des modèle pour le bouton de téléchargement de la vCard et
du rendez-vous ICS dans un article.

```
<bouton_vcard
  |nom=Martin
  |prenom=Jean-Charles
  |employeur=Alstom
  |fonction=Directeur technique
  |telhome=+33142729663
  |telwork=+33612345678
  |mailwork=jcmartin@alstom.com
  |adrhome=13 rue Charles Vincennes;75015;PARIS;;France
  |site=http://blog.martin.net/
  |naissance=12/3/1998
  |note=je suis une note sur
plusieurs lignes.
Encore une petite pour la route.
  |label=Ma belle vCard
  |icone=article-xx.svg
>
```

```
<bouton_rdv
  |debut=9/3/2022 10:00
  |fin=9/3/2022 11:00
  |titre=Mon beau RDV
  |lieu=Chez mon pote Roger
  |description=je suis une description sur
plusieurs lignes. Cette ligne doit dépasser les 75 caractères pour tester la découpe. Et même pire cette ligne va être plus grande que deux fois afin de tester encore mieux.
Encore une petite pour la route.
  |label=Mon RDV
  |icone=agenda-xx.svg
>
```

Un exemple de bouton pour un rdv Google manuel à ajouter par l'intermédiaire d'une page dédiée.

```
<bouton_rdv_google
  |debut=9/3/2022 10:00
  |fin=9/3/2022 11:00
  |titre=Mon beau RDV
  |lieu=Chez mon pote Roger
  |description=je suis une description sur
plusieurs lignes. Cette ligne doit dépasser les 75 caractères pour tester la découpe. Et même pire cette ligne va être plus grande que deux fois afin de tester encore mieux.
Encore une petite pour la route.
  |label=Mon RDV Google
  |icone=agenda-xx.svg
>
```

TODO :
- Ajouter une configuration de la version de la vcard (3 ou 4)
