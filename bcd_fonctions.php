<?php
/**
 * Filtres utiles au plugin Boutons vCard & Rendes-vous.
 *
 * @plugin     bcd
 *
 * @copyright  2021
 * @author     Thrax
 *
 * @licence    MIT
 **/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Crée le fichier vCard et retourne son chemin.
 *
 * @api
 *
 * @param array $elements Tableau des éléments permettant le calcul du fichier vcard
 * @param bool  $forcer   Indique si true de forcer la génération de la vcard
 *
 * @return string Chemin du fichier cache représentatif de la vcard
 */
function vcard_generer($elements, $forcer = false) {
	// On vérifie et formate le tableau des données de la vCard
	include_spip('inc/bcd');
	$elements = vcard_initialiser($elements);

	// Identification du cache de la vcard
	include_spip('inc/ezcache_cache');
	$cache = [
		'nom' => rdv_cache_formater_titre($elements['nom']),
	];
	if (!empty($elements['prenom'])) {
		$cache['prenom'] = rdv_cache_formater_titre($elements['prenom']);
	}

	// On calcule le chemin du cache car de toute façon il faut le renvoyer
	// -> on utilisera donc cet identifiant pour l'appel des fonctions de cache plutôt que le tableau
	$fichier_cache = cache_nommer('bcd', 'vcard', $cache);

	// Si le cache n'existe pas ou si on a demandé le forçage, on le crée,
	// sinon on transmet uniquement son chemin complet
	if (
		$forcer
		or !cache_est_valide('bcd', 'vcard', $fichier_cache)
	) {
		// On constitue le contenu de la vcard en utilisant l'inclusion associée
		$vcard = recuperer_fond('inclure/vcard30', $elements);

		// On crée le cache et on renvoie une chaine vide en cas d'erreur
		if (!cache_ecrire('bcd', 'vcard', $fichier_cache, $vcard)) {
			$fichier_cache = '';
		}
	}

	return  $fichier_cache;
}

/**
 * Compile la balise `#AUTEUR_INFOS_VCARD` qui calcule le tableau des informations de l'auteur nécessaires à
 * la création de la vCard.
 * La signature de la balise est : `#AUTEUR_INFOS_VCARD{id_auteur[, forcer]}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_AUTEUR_INFOS_VCARD_dist($p) {
	// Récupération des arguments.
	$id_auteur = interprete_argument_balise(1, $p);
	$id_auteur = $id_auteur ?? '0';
	$forcer = interprete_argument_balise(2, $p);
	$forcer = isset($forcer) ? str_replace('\'', '"', $forcer) : '"non"';

	// On appelle la fonction de calcul des informations
	$p->code = "calculer_infos_vcard({$id_auteur}, {$forcer})";

	return $p;
}

function calculer_infos_vcard($id_auteur, $forcer) {
	// On renvoie le résultat de la fonction d'extraction des données vCard
	include_spip('inc/bcd');

	return vcard_auteur_informer((int) $id_auteur, $forcer === 'oui');
}

/**
 * Crée le fichier ICS et retourne son chemin.
 *
 * @api
 *
 * @param array $elements Tableau des éléments permettant le calcul du fichier ics
 * @param bool  $forcer   Indique si true de forcer la génération de la vcard
 *
 * @return string Chemin du fichier cache représentatif de l'évenement de calendrier
 */
function rdv_generer($elements, $forcer = false) {
	// Dans le cas où l'on créer le fichier ics d'un évènement du plugin Agenda il est possible que le tableau des
	// éléments soit vide si le plugin n'est pas actif.
	$fichier_cache = '';
	if ($elements) {
		// On vérifie et formate le tableau des données du rdv
		include_spip('inc/bcd');
		$elements = rdv_initialiser($elements);

		// Identification du cache du fichier ICS
		include_spip('inc/ezcache_cache');
		$cache = [
			'debut' => rdv_cache_formater_date($elements['debut']),
			'fin'   => rdv_cache_formater_date($elements['fin']),
		];
		// si l'élément facultatif titre est transmis, on l'utilise
		if ($elements['titre']) {
			$cache['titre'] = rdv_cache_formater_titre($elements['titre']);
		}

		// On calcule le chemin du cache car de toute façon il faut le renvoyer
		// -> on utilisera donc cet identifiant pour l'appel des fonctions de cache plutôt que le tableau
		$fichier_cache = cache_nommer('bcd', 'rdv', $cache);

		// Si le cache n'existe pas on le crée, sinon on transmet uniquement son chemin complet
		if (
			$forcer
			or !cache_est_valide('bcd', 'rdv', $fichier_cache)
		) {
			// On constitue le contenu de la vcard en utilisant l'inclusion associée
			$rdv = recuperer_fond('inclure/icalevent20', $elements);

			// On crée le cache et on renvoie une chaine vide en cas d'erreur
			if (!cache_ecrire('bcd', 'rdv', $fichier_cache, $rdv)) {
				$fichier_cache = '';
			}
		}
	}

	return $fichier_cache;
}

/**
 * Compile la balise `#EVENEMENT_INFOS_RDV` qui calcule le tableau des informations de l'évènement Agenda nécessaires à
 * la création du rendez-vous.
 * La signature de la balise est : `#EVENEMENT_INFOS_RDV{id_evenement[, forcer]}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_EVENEMENT_INFOS_RDV_dist($p) {
	// Récupération des arguments.
	$id_evenement = interprete_argument_balise(1, $p);
	$id_evenement = $id_evenement ?? '0';
	$forcer = interprete_argument_balise(2, $p);
	$forcer = isset($forcer) ? str_replace('\'', '"', $forcer) : '"non"';

	// On appelle la fonction de calcul des informations
	$p->code = "calculer_infos_rdv({$id_evenement}, {$forcer})";

	return $p;
}

function calculer_infos_rdv($id_evenement, $forcer) {
	// On renvoie le résultat de la fonction d'extraction des données de rdv
	include_spip('inc/bcd');

	return rdv_evenement_informer((int) $id_evenement, $forcer === 'oui');
}

/**
 * Calcule et retourne l'url de page d'ajout de l'évènement à un calendrier Google.
 *
 * @api
 *
 * @param array $elements Tableau des éléments de définition de l'évènement
 *
 * @return string URL Google de la page d'ajout
 */
function rdv_google_generer($elements) {
	// On vérifie et formate le tableau des données du rdv
	include_spip('inc/bcd');
	$elements = rdv_initialiser($elements);

	// Construction de l'url de la page Google permettant l'ajout de l'évènement
	$url_event = 'https://www.google.com/calendar/event?action=TEMPLATE';

	// Dans une url GoogleAgenda, les dates de l'événement sont transmises au format UTC
	// c'est la TimeZone dans laquelle l'agenda est défini qui calcule le décalage
	// et place le rendez-vous de façon adhoc

	// La fonction date_normaliser_ical retourne la date en lui retirant
	// la timezone donnée par le serveur (le serveur la définie dans
	// .htaccess ou php.ini. On y accède par ini_get('date.timezone'))

	// Si la Timezone n'est pas définie par le serveur, elle sera considérée comme déjà en UTC
	// aussi, $elements['tz_serveur'] permet d'indiquer quelle est la TimeZone de la date traitée.
	// Liste des Fuseaux Horaires Supportés -> https://www.php.net/manual/fr/timezones.php
	// $elements['tz_calendrier'] permet de préciser la Timezone du calendrier.

	$debut = date_normaliser_ical($elements['debut'], $elements['tz_serveur']);
	$fin = date_normaliser_ical($elements['fin'], $elements['tz_serveur']);

	$url_event .= '&dates=' . $debut . '/' . $fin;

	if ($elements['tz_calendrier']) {
		$url_event .= '&ctz=' . $elements['tz_calendrier'];
	}

	// Ajout des éléments textuels titre, lieu et description si besoin
	if ($elements['titre']) {
		$url_event .= '&text=' . rawurlencode($elements['titre']);
	}
	if ($elements['lieu']) {
		$url_event .= '&location=' . urlencode($elements['lieu']);
	}
	if ($elements['description']) {
		$url_event .= '&details=' . rawurlencode($elements['description']);
	}

	return $url_event;
}

/**
 * Normalise le texte d'une propriété de vCard ou de d'évènement iCalendar en supprimant les caractères interdits
 * et en découpant le texte en plusieurs lignes de 75 caractères au maximum.
 *
 * @param string $valeur    La valeur de la propriété de type texte
 * @param string $propriete Le nom de la propriété en majuscules
 *
 * @return string La ligne complète normalisée prête à être insérée.
 */
function ligne_normaliser($valeur, $propriete) {
	// On constitue la ligne avec la propriété et sa valeur (on n'applique cette normalisation que
	// sur des lignes de ce type)
	$ligne = strtoupper($propriete) . ':' . $valeur;

	// Si la ligne comporte des br (par exemple un texte spip) ou des retours/fins de ligne
	// on les remplace par le caractère linefeed '\n' afin que le décodage de la vCard restaure les CRLF.
	$retours = ["\r\n", "\n\r", "\n", "\r", '<br>', '<BR>', '<br/>', '<BR/>', '<br />', '<BR />'];
	$ligne = str_replace($retours, '\n', $ligne);

	//Suppression des caractères interdits
	$ligne = preg_replace('/[\x00-\x1F\x7F\xA0]/u', '', $ligne);

	// Découpe de la ligne en sous-lignes de 75 caractères max
	$ligne_normalisee = '';
	$no_sousligne = 0;
	while (strlen($ligne) > 0) {
		// Taille maximale d'une sous-ligne suivant que c'est la première ou les suivantes
		$longueur_max = $no_sousligne === 0 ? 75 : 74;

		// Calcul de la taille de la sous-ligne
		$lg_sousligne = (strlen($ligne) > $longueur_max ? $longueur_max : strlen($ligne));
		if ($no_sousligne > 0) {
			// Si on est sur une sous-ligne on ajoute un espace en premier caractère pour indiquer la continuité
			$ligne_normalisee .= ' ';
		}
		// On constitue la sous-ligne complète avec un CRLF
		$ligne_normalisee .= substr($ligne, 0, $lg_sousligne) . "\r\n";

		// On passe à la sous-ligne suivante si elle existe
		++$no_sousligne;
		$ligne = substr($ligne, $longueur_max);
	}

	return $ligne_normalisee;
}

/**
 * Adapte une date pour être insérée dans une valeur de date d'un export ICS.
 * La date renvoyée est en UTC et au format `Ymd\THis\Z`, tel que '20150428T163254Z'.
 *
 * Cette fonction contourne provisoirement le souci du filtre spip date_ical() qui ne fonctionne pas si les
 * secondes ne sont pas fournies dans la chaine d'entrée.
 *
 * @api
 *
 * @param $date
 * @param mixed $tz
 *
 * @return false|string La date au format iCal.
 */
function date_normaliser_ical($date, $tz = '') {
	// Traiter le décalage
	if ($tz) {
		date_default_timezone_set($tz);
	}

	// Traiter l'heure
	include_spip('inc/bcd');
	[$heures, $minutes, $secondes] = heure_extraire($date);

	// Traiter la date
	include_spip('inc/filtres_dates');
	[$annee, $mois, $jour] = recup_date($date);

	return gmdate('Ymd\THis\Z', mktime($heures, $minutes, $secondes, $mois, $jour, $annee));
}
