<?php
/**
 * Fonctions utilitaires du plugin Boutons vCard & Rendez-vous.
 *
 * @plugin     bcd
 *
 * @copyright  2021
 * @author     Thrax
 *
 * @licence    MIT
 **/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_BCD_VCARD_DONNEES_UNITAIRES')) {
	/**
	 * Liste données d'une vcard de type string (cardinalité 0 ou 1).
	 */
	define(
		'_BCD_VCARD_DONNEES_UNITAIRES',
		[
			'nom',
			'prenom',
			'nom_add',
			'nom_prefixe',
			'employeur',
			'fonction',
			'site',
			'naissance',
			'note',
		]
	);
}

if (!defined('_BCD_VCARD_DONNEES_MULTIPLES')) {
	/**
	 * Liste données d'une vcard de type array (cardinalité 0 à n).
	 */
	define(
		'_BCD_VCARD_DONNEES_MULTIPLES',
		[
			'mails',
			'tels',
			'adrs'
		]
	);
}

if (!defined('_BCD_VCARD_SOUS_DONNEES_ADRESSE')) {
	/**
	 * Liste données d'une adresse de vcard.
	 * ordre : Boîte postale, Adresse étendue, Nom de rue, Ville, Région (ou état/province), Code postal et Pays.
	 */
	define(
		'_BCD_VCARD_SOUS_DONNEES_ADRESSE',
		[
			'rue',
			'ville',
			'region',
			'code',
			'pays',
		]
	);
}

if (!defined('_BCD_VCARD_TYPES_MULTIPLES')) {
	/**
	 * Liste types de données multiples correspondant à un index dans le tableau de la vcard.
	 */
	define(
		'_BCD_VCARD_TYPES_MULTIPLES',
		[
			'home',
			'work',
		]
	);
}

if (!defined('_BCD_RDV_DONNEES_UNITAIRES')) {
	/**
	 * Liste données d'un rendez-vous (cardinalité 0 ou 1).
	 */
	define(
		'_BCD_RDV_DONNEES_UNITAIRES',
		[
			'debut',
			'fin',
			'titre',
			'description',
			'lieu',
			'tz_calendrier',
			'tz_serveur'
		]
	);
}

/**
 * Extrait toutes les données d'un auteur utiles à la constitution de sa vCard.
 * Si l'auteur est complété par les plugin Contacts & Organisations et Coordonnées, les données afférentes
 * sont transmises.
 *
 * @internal
 *
 * @param int  $id_auteur Identifiant de l'auteur dont on veut extraire les données pour la vcard
 * @param bool $forcer    Indique si true de forcer la relecture complète des informations
 *
 * @return array Tableau des éléments de la vCard.
 */
function vcard_auteur_informer($id_auteur, $forcer = false) {
	// Initialiser le tableau des éléments de la vcard de l'auteur
	static $auteurs = [];

	if (
		$forcer
		or !isset($auteurs[$id_auteur])
	) {
		// On initialise les informations brutes à transmettre pour créer la vCard
		$vcard = [];

		// Si l'auteur est dans un statut adéquat (hors poubelle, hors nouveau)
		$statuts_exclus = [
			'nouveau',
			'5poubelle',
		];
		$where = [
			"id_auteur={$id_auteur}",
			sql_in('statut', $statuts_exclus, 'NOT')
		];
		if ($auteur = sql_fetsel('*', 'spip_auteurs', $where)) {
			// On commence toujours par récupérer et initialiser les données basiques provenant de l'auteur
			// -- le nom spip ne correspond pas forcément au nom mais souvent au nom/prénom voire à un pseudo
			$vcard['nom'] = $auteur['nom'];
			// -- on considère que le mail spip est celui du domicile
			$vcard['mails'] = ['home' => $auteur['email']];
			// -- on intègre l'url du site de l'auteur
			$vcard['site'] = $auteur['url_site'];
			// -- on classe l'url de l'auteur dans les notes
			$vcard['note'] = generer_url_ecrire('auteur', "id_auteur={$id_auteur}");
			$vcard['note'] = str_replace('&amp;', '&', $vcard['note']);

			// Si le plugin Contacts & Organisations est installé et que l'auteur est devenu un contact,
			// on utilise les données de contact en addition ou remplacement de celles de l'auteur
			if (defined('_DIR_PLUGIN_CONTACTS')) {
				// Recherche du contact
				if ($contact = sql_fetsel('*', 'spip_contacts', ["id_auteur={$id_auteur}"])) {
					// -- on écrase le nom et on ajoute le prénom et la civilité
					$vcard['nom'] = $contact['nom'];
					$vcard['prenom'] = $contact['prenom'];
					$vcard['nom_prefixe'] = $contact['civilite'];
					// -- la fonction de la personne
					$vcard['fonction'] = $contact['fonction'];
					// -- on ajoute la date de naissance
					$vcard['naissance'] = $contact['date_naissance'];
					// -- on complète la note avec le descriptif
					$vcard['note'] = $contact['descriptif'] . "\r\n" . $vcard['note'];
				}
			}

			// Si le plugin Coordonnées est installé on utilise les données de coordonnées en addition
			// de celles de l'auteur
			if (defined('_DIR_PLUGIN_COORDONNEES')) {
				// Recherche des numéros de téléphone
				// -- les numéros sont associés à l'auteur dans une table de liens qui contient aussi leur type qui vaut
				//    home ou work pour ceux qui sont utiles à bcd.
				// -- le numéro lui est dans la table spip_numeros, il est donc nécessaire de faire une jointure.
				$from = ['spip_numeros_liens as liens', 'spip_numeros as numeros'];
				$select = ['liens.type as type', 'numeros.numero as numero'];
				$where = [
					'liens.objet=' . sql_quote('auteur'),
					'liens.id_objet=' . $id_auteur,
					sql_in('liens.type', _BCD_VCARD_TYPES_MULTIPLES),
					'liens.id_numero=numeros.id_numero'
				];
				if ($tels = sql_allfetsel($select, $from, $where)) {
					// -- on ajoute les numéros
					foreach ($tels as $_tel) {
						$vcard['tels'][$_tel['type']] = $_tel['numero'];
					}
				}

				// Recherche des adresses mails
				// -- les mails sont associés à l'auteur dans une table de liens qui contient aussi leur type qui vaut
				//    home ou work pour ceux qui sont utiles à bcd.
				// -- le mail lui est dans la table spip_emails, il est donc nécessaire de faire une jointure.
				$from = ['spip_emails_liens as liens', 'spip_emails as emails'];
				$select = ['liens.type as type', 'emails.email as email'];
				$where = [
					'liens.objet=' . sql_quote('auteur'),
					'liens.id_objet=' . $id_auteur,
					sql_in('liens.type', _BCD_VCARD_TYPES_MULTIPLES),
					'liens.id_email=emails.id_email'
				];
				if ($mails = sql_allfetsel($select, $from, $where)) {
					// -- on ajoute les mails ou on remplace le mail personnel provenant de l'auteur si il a été
					//    redéfini pour le contact
					foreach ($mails as $_mail) {
						$vcard['mails'][$_mail['type']] = $_mail['email'];
					}
				}

				// Recherche des adresses postales
				// -- les adresses sont associés à l'auteur dans une table de liens qui contient aussi leur type qui vaut
				//    home ou work pour ceux qui sont utiles à bcd.
				// -- l'adresse elle est dans la table spip_adresses, il est donc nécessaire de faire une jointure.
				$from = ['spip_adresses_liens as liens', 'spip_adresses as adresses'];
				$select = [
					'liens.type as type',
					'adresses.voie as rue',
					'adresses.complement as rue_add',
					'adresses.code_postal as code',
					'adresses.ville as ville',
					'adresses.pays as code_pays',
				];
				$where = [
					'liens.objet=' . sql_quote('auteur'),
					'liens.id_objet=' . $id_auteur,
					sql_in('liens.type', _BCD_VCARD_TYPES_MULTIPLES),
					'liens.id_adresse=adresses.id_adresse'
				];
				if ($adrs = sql_allfetsel($select, $from, $where)) {
					// -- on ajoute les adresses en considérant que le complément permet de finaliser l'index rue
					include_spip('action/editer_objet');
					foreach ($adrs as $_adr) {
						$rue = $_adr['rue'] . ($_adr['rue_add'] ? "\r\n" . $_adr['rue_add'] : '');
						$pays = objet_lire(
							'pays',
							$_adr['code_pays'],
							[
								'champ_id' => 'code',
								'champs'   => 'nom'
							]
						);
						$vcard['adrs'][$_adr['type']] = [
							'rue'   => $rue,
							'code'  => $_adr['code'],
							'ville' => $_adr['ville'],
							'pays'  => $pays ? extraire_multi($pays) : '',
						];
					}
				}
			}
		}

		// Sauvegarde des informations de l'auteur
		$auteurs[$id_auteur] = $vcard;
	}

	return $auteurs[$id_auteur];
}

/**
 * Initialise les éléments qui sont fournis au squelette de création du fichier VCF.
 *
 * @internal
 *
 * @param array $elements Tableau des éléments permettant le calcul du fichier vcard
 *
 * @return array Tableau des éléments de la vCard correctement initialisés et formatés
 */
function vcard_initialiser($elements) {
	// Initialisation de l'ensemble des données exportables dans une vCard
	$vcard = [];

	// Acquisition et nettoyage des données unitaires
	foreach (_BCD_VCARD_DONNEES_UNITAIRES as $_donnee) {
		$vcard[$_donnee] = isset($elements[$_donnee]) ? trim($elements[$_donnee]) : '';
	}

	// On formate les données unitaires conformément à la RFC vCard
	// -- pour la note la normalisation du texte est fait dans le squelette, sur la ligne complète
	// -- la date de naissance en Ymd
	include_spip('inc/filtres');
	$vcard['naissance'] = $vcard['naissance'] === '0000-00-00 00:00:00'
		? ''
		: affdate($vcard['naissance'], 'Y-m-d');

	// On construit les données multiples provenant
	// - soit d'un tableau (appel PHP)
	// - soit d'éléments unitaires (appel par un modèle inclus dans un article
	// Pour chacune de ces données on distingue uniquement le type domicile (home) ou travail (work)
	foreach (_BCD_VCARD_DONNEES_MULTIPLES as $_donnee) {
		$vcard[$_donnee] = [];
		if (!empty($elements[$_donnee])) {
			// On filtre les index avec les types admis et on merge dans le tableau final
			$vcard[$_donnee] = array_intersect_key($elements[$_donnee], array_flip(_BCD_VCARD_TYPES_MULTIPLES));

			if ($_donnee === 'adrs') {
				// Pour les adresses qui sont des tableaux, il faut en plus normaliser ce tableau
				foreach ($vcard[$_donnee] as $_type => $_adresse) {
					foreach (_BCD_VCARD_SOUS_DONNEES_ADRESSE as $_element_adresse) {
						$vcard[$_donnee][$_type][$_element_adresse] = isset($_adresse[$_element_adresse])
							? trim($_adresse[$_element_adresse])
							: '';
					}

					// Pour simplifier, on ajoute aussi l'adresse complète intégrable telle que dans le squelette de
					// la vCard
					$vcard[$_donnee][$_type]['adresse'] = vcard_composer_adresse($vcard[$_donnee][$_type]);
				}
			}
		} else {
			// Il est possible (par exemple dans un appel par modèle au sein d'un article) de passer les données
			// en utilisant des variables unitaires par type, à savoir, mailhome et mailwork pour les mails
			// -- abréviation de la donnée
			$abreviation = substr($_donnee, 0, -1);
			foreach (_BCD_VCARD_TYPES_MULTIPLES as $_type) {
				$index = "{$abreviation}{$_type}";
				if (!empty($elements[$index])) {
					if (
						($_donnee === 'mails')
						and ($mail = email_valide($elements[$index]))
					) {
						// Vérifier la validité d'une adresse email
						$vcard[$_donnee][$_type] = $mail;
					} elseif ($_donnee === 'tels') {
						// pas de vérification sur les numéros de téléphone
						$vcard[$_donnee][$_type] = $elements[$index];
					} else {
						// Traitement des sous-élements d'une adresse postale.
						// On considère que l'adresse est fournie de la manière suivante:
						// rue;code;ville;region;pays
						// Pour simplifier le squelette, on ajoute aussi l'adresse complète intégrable telle que
						// dans la vCard
						$adresse = explode(';', $elements[$index]);
						foreach ($adresse as $_cle => $_element_adresse) {
							$sous_type = _BCD_VCARD_SOUS_DONNEES_ADRESSE[$_cle];
							$vcard[$_donnee][$_type][$sous_type] = trim($_element_adresse);
						}

						// Pour simplifier, on ajoute aussi l'adresse complète intégrable telle que dans le squelette de
						// la vCard
						$vcard[$_donnee][$_type]['adresse'] = vcard_composer_adresse($vcard[$_donnee][$_type]);
					}
				}
			}
		}
	}

	return  $vcard;
}

/**
 * Calcule la partie valeur de la ligne d'une adresse.
 *
 * @param array $elements Tableau des éléments normalisés composant une adresse.
 *
 * @return string
 */
function vcard_composer_adresse($elements) {
	$adresse = ';';

	foreach (_BCD_VCARD_SOUS_DONNEES_ADRESSE as $_donnee_adr) {
		$adresse .= ';' . ($elements[$_donnee_adr] ?? '');
	}

	return $adresse;
}

/**
 * Extrait toutes les données d'un évènement d'agenda utiles à la constitution de son rdv iCal.
 *
 * @internal
 *
 * @param int  $id_evenement Identifiant de l'évènement dont on veut extraire les données pour le rdv iCal
 * @param bool $forcer       Indique si true de forcer la relecture complète des informations
 *
 * @return array Tableau des éléments du rdv iCal.
 */
function rdv_evenement_informer($id_evenement, $forcer = false) {
	// Initialiser le tableau des éléments du rdv ical de l'évènement
	static $evenements = [];

	if (
		$forcer
		or !isset($evenements[$id_evenement])
	) {
		// On initialise les informations brutes à transmettre pour créer le rdv
		$rdv = [];

		// L'existence d'un évènement est subordonné à l'activation du plugin Agenda
		if (defined('_DIR_PLUGIN_AGENDA')) {
			// Si l'evenement est dans un statut adéquat (hors poubelle, hors nouveau)
			$statuts_exclus = [
				'nouveau',
				'5poubelle',
			];
			$where = [
				"id_evenement={$id_evenement}",
				sql_in('statut', $statuts_exclus, 'NOT')
			];
			if ($evenement = sql_fetsel('*', 'spip_evenements', $where)) {
				// On commence toujours par récupérer et initialiser les données basiques provenant de l'evenement
				// -- les évènements du plugin Agenda sont au format iCal ce qui permet de copier simplement les éléments.
				$rdv['debut'] = $evenement['date_debut'];
				$rdv['fin'] = $evenement['date_fin'];
				$rdv['titre'] = $evenement['titre'];
				$rdv['lieu'] = $evenement['lieu'];
				if ($evenement['adresse']) {
					$rdv['lieu'] .= ', ' . trim(textebrut(preg_replace(',\s+,', ' ', $evenement['adresse'])));
				}
				$rdv['description'] = $evenement['descriptif'];
			}
		}

		// Sauvegarde des informations de l'evenement
		$evenements[$id_evenement] = $rdv;
	}

	return $evenements[$id_evenement];
}

/**
 * Initialise les éléments qui sont fournis au squelette de création du fichier ICS.
 *
 * @internal
 *
 * @param array $elements Tableau des éléments permettant le calcul du fichier vcard
 *
 * @return array Tableau des éléments du RDV correctement initialisés et formatés
 */
function rdv_initialiser($elements) {
	// Initialisation de l'ensemble des données exportables dans une vCard
	$rdv = [];

	// Acquisition et nettoyage des données unitaires
	foreach (_BCD_RDV_DONNEES_UNITAIRES as $_donnee) {
		$rdv[$_donnee] = isset($elements[$_donnee]) ? trim($elements[$_donnee]) : '';
	}

	return  $rdv;
}

/**
 * Formate une date pour l'insérer dans le nom d'un cache en restant lisible.
 *
 * @param string $date Chaîne de date
 *
 * @return string La date formatée
 */
function rdv_cache_formater_date($date) {
	// Traiter l'heure et la date séparément
	include_spip('inc/filtres_dates');
	[$heures, $minutes, $secondes] = heure_extraire($date);
	[$annee, $mois, $jour] = recup_date($date);

	// Formater la date en Ymd-hi
	return date('Ymd-Hi', mktime($heures, $minutes, $secondes, $mois, $jour, $annee));
}

/**
 * Formate un titre pour l'insérer dans le nom d'un cache en restant lisible.
 *
 * @param string $titre Chaîne du titre
 *
 * @return string Le titre formatée
 */
function rdv_cache_formater_titre($titre) {
	// Transformer le titre en slug en utilisant le séparateur tiret pour ne pas entrer en collision avec
	// le séparateur des composants du cache
	include_spip('inc/filtres');
	$options_normaliser = [
		'separateur' => '-',
	];

	return identifiant_slug($titre, '', $options_normaliser);
}

/**
 * Retrouve à partir d'une chaîne les valeurs heures, minutes, secondes.
 * Par exemple, la fonction sait extraire un foraire des chaine `11:29:55` ou `11:29`.
 *
 * Cette fonction contourne provisoirement le souci du filtre recup_heure qui ne fonctionne pas si les
 * secondes ne sont pas fournies dans la chaine d'entrée.
 *
 * @param string $date Chaîne de date contenant éventuellement une horaire
 *
 * @return array
 *               - [heures, minutes, secondes] si une herure a été identifiée
 *               - [0, 0, 0] sinon
 */
function heure_extraire($date) {
	// Traiter l'heure
	static $heure = [0, 0, 0];
	if (preg_match('#([0-9]{1,2}):([0-9]{1,2})(?::([0-9]{1,2}))?#', $date, $elements)) {
		array_shift($elements);
		if (!isset($elements[2])) {
			$elements[2] = 0;
		}
		$heure = $elements;
	}

	return $heure;
}
