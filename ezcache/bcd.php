<?php
/**
* Ce fichier contient la configuration des caches de bcd basés sur l'API de Cache Factory.
*/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
* Renvoie la configuration spécifique des caches de bcd.
*
* @param string $plugin
*        Identifiant qui permet de distinguer le module appelant qui peut-être un plugin ou
*        un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
*
* @return array
*        Tableau de la configuration des caches du plugin bcd.
*/
function bcd_cache_configurer($plugin) {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Check Factory.
	return [
		'rdv' => [
			'racine'            => '_DIR_TMP',
			'sous_dossier'      => true,
			'sous_dossier_auto' => true,
			'nom_prefixe'       => '',
			'nom_obligatoire'   => ['debut', 'fin'],
			'nom_facultatif'    => ['titre'],
			'extension'         => '.ics',
			'securisation'      => false,
			'serialisation'     => false,
			'decodage'          => false,
			'separateur'        => '_',
			'conservation'      => 0
		],
		'vcard' => [
			'racine'            => '_DIR_TMP',
			'sous_dossier'      => true,
			'sous_dossier_auto' => true,
			'nom_prefixe'       => '',
			'nom_obligatoire'   => ['nom'],
			'nom_facultatif'    => ['prenom'],
			'extension'         => '.vcf',
			'securisation'      => false,
			'serialisation'     => false,
			'decodage'          => false,
			'separateur'        => '_',
			'conservation'      => 0
		],
	];
}

/**
 * Complète la description canonique d'un cache de rendez-vous.
 * Le plugin Boutons vCard & Rendez-vous rajoute, pour le cache rdv, les dates de début et fin dans un format lisible
 * qui sera utilisé pour afficher la liste des caches dans le formulaire de vidage.
 *
 * @param string $plugin        Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                              ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param array  $cache         Tableau identifiant le cache pour lequel on veut construire le nom.
 * @param string $fichier_cache Fichier cache désigné par son chemin complet.
 * @param array  $configuration Configuration complète des caches du plugin utilisateur lue à partir de la meta de stockage.
 *
 * @return array Description du cache complétée par un ensemble de données propres au plugin.
 */
function bcd_rdv_cache_completer($plugin, $cache, $fichier_cache, $configuration) {
	// On rajoute les dates en version lisible pour un éventuel affichage.
	include_spip('inc/bcd');
	foreach (['debut', 'fin'] as $_date) {
		// On construit une date sous la forme '20151104111420' avec les secondes
		$date = str_replace('-', '', $cache[$_date]) . '00';
		$cache["{$_date}_lisible"] = affdate($date, 'd/m/Y H:i');

		// Si la date est la même on réduit la fin à l'horaire
		if (
			($_date === 'fin')
			and (substr($cache['debut_lisible'], 0, 11) === substr($cache['fin_lisible'], 0, 11))
		) {
			$cache["{$_date}_lisible"] = affdate($date, 'H:i');
		}
	}

	return $cache;
}
