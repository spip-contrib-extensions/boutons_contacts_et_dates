<?php
/**
 * Fichier gérant l'utilisation des pipelines par le plugin.
 *
 * @plugin     bcd
 *
 * @copyright  2021
 * @author     Thrax
 **/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Afficher des infos dans la boite de l'objet.
 * - Pour un auteur, on affiche un bouton correspondant à sa vCard.
 * - Pour un évènement d'agenda, on affiche un bouton correspondant à son rdv iCal.
 *
 * @pipeline boite_infos
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline mises à jour
**/
function bcd_boite_infos($flux) {
	if (
		isset($flux['args']['type'])
		and in_array($flux['args']['type'], ['auteur', 'evenement'])
		and isset($flux['args']['id'])
		and ($id_objet = (int) ($flux['args']['id']))
		and include_spip('inc/autoriser')
		and include_spip('inc/bcd')
	) {
		$type_objet = $flux['args']['type'];
		if (
			($type_objet === 'auteur')
			and autoriser('exportervcard', 'auteur', $id_objet)
		) {
			// Rechercher les informations sur l'auteur
			$vcard = vcard_auteur_informer($id_objet);

			// Insertion du bouton dans le HTML si la vcard est remplie
			if ($vcard) {
				$contexte = [
					'args' => $vcard,
				];
				$flux['data'] .= recuperer_fond('modeles/bouton_vcard', $contexte);
			}
		} elseif (
			($type_objet === 'evenement')
			and autoriser('exporterrdv', 'evenement', $id_objet)
		) {
			// Rechercher les informations sur l'auteur
			$rdv = rdv_evenement_informer($id_objet);

			// Insertion du bouton dans le HTML si la vcard est remplie
			if ($rdv) {
				$contexte = [
					'args' => $rdv,
				];
				$flux['data'] .= recuperer_fond('modeles/bouton_rdv', $contexte);
			}
		}
	}

	return $flux;
}

/**
 * Générer un nouveau cache de vCard suite à la modification d'une information liée à un auteur.
 *
 * @pipeline boite_infos
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline mises à jour
**/
function bcd_post_edition($flux) {
	static $objets_concernes = [
		'auteur',
		'contact',
		'email',
		'numero',
		'adresse',
		'evenement'
	];

	if (
		isset($flux['args']['type'])
		and (in_array($flux['args']['type'], $objets_concernes))
		and ($objet = $flux['args']['type'])
		and isset($flux['args']['id_objet'])
	) {
		// On détermine l'id du ou des auteurs:
		// -- pour un objet auteur ou contact le lien est unique
		// -- pour un email, un numéro de téléphone ou une adresse postale, il est possible que plusieurs auteurs
		//    soient concernés
		include_spip('inc/bcd');
		$id_evenement = 0;
		$ids_auteur = [];
		if ($objet === 'evenement') {
			$id_evenement = (int) ($flux['args']['id_objet']);
		} elseif ($objet === 'auteur') {
			$ids_auteur[] = (int) ($flux['args']['id_objet']);
		} elseif ($objet === 'contact') {
			// On récupère l'id du contact ainsi que l'id de l'auteur
			include_spip('action/editer_objet');
			$id_contact = $flux['args']['id_objet'];
			$ids_auteur[] = (int) (objet_lire('contact', $id_contact, ['champs' => 'id_auteur']));
		} else {
			// emails, téléphones et adresses : id de l'objet concerné, nom de table et nom du champ id
			include_spip('base/objets');
			$id_objet = (int) ($flux['args']['id_objet']);
			$table = table_objet_sql($objet);
			$id_table = id_table_objet($objet);

			// Identifier les auteurs liés en se limitant aux types utilisés uniquement
			$where = [
				'objet=' . sql_quote('auteur'),
				sql_in('type', _BCD_VCARD_TYPES_MULTIPLES),
				$id_table . '=' . $id_objet
			];
			if ($auteurs = sql_allfetsel('id_objet', "{$table}_liens", $where)) {
				$ids_auteur = array_column($auteurs, 'id_objet');
			}
		}

		if ($ids_auteur) {
			foreach ($ids_auteur as $_id_auteur) {
				// Extraire les informations de l'auteur nécessaires à la vCard
				if ($vcard = vcard_auteur_informer($_id_auteur, true)) {
					// Forcer la regénération de la vcard
					include_spip('inc/bcd_fonctions');
					vcard_generer($vcard, true);
				}
			}
		}

		if ($id_evenement) {
			if ($rdv = rdv_evenement_informer($id_evenement, true)) {
				// Forcer la regénération du rdv iCal
				include_spip('inc/bcd_fonctions');
				rdv_generer($rdv, true);
			}
		}
	}

	return $flux;
}
